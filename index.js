// 创建动态光标
const loadingSpinner = require('loading-spinner');
// 创建进度条
const cliProgress = require('cli-progress')
const progressBar = new cliProgress.SingleBar({
    format: '\x1B[1;35m下载进度： {bar} {percentage}% | {value}/{total}\x1B[0m'
}, cliProgress.Presets.shades_classic);
// 解压
const compressing = require("compressing");
const request = require('request')
const fs = require('fs')
const path = require('path')

//重写系统打印，增加颜色
const {
    log
} = console
console.log = info => log('\x1B[1;32m%s\x1B[0m', info)
console.error = info => log('\x1B[1;31m%s\x1B[0m', info)
class Upicon {
    constructor(option) {
        this.url = option.url
        this.headers = option.headers
        // icon文件位置
        this.basePath = option.basePath;
        // 下载文件名
        this.fileName = option.fileName
        // 下载文件全路径
        this.downloadFilePath = path.join(this.basePath, this.fileName)
        this.whiteArr = ['demo_index.html', 'demo.css', 'iconfont.css', 'iconfont.js', 'iconfont.json', 'iconfont.ttf', 'iconfont.woff', 'iconfont.woff2'] // 文件夹中的不能删除文件白名单
    }
    // 文件下载
    downloadFile() {
        // 判断文件夹是否存在
        try {
            const state = fs.statSync(this.basePath).isDirectory()
            if (!state) fs.mkdirSync(this.basePath)
        } catch (error) {
            fs.mkdirSync(this.basePath)
        }
        // 如果存在下载文件，先删除避免冲突
        try {
            fs.unlinkSync(this.downloadFilePath); // 删除下载文件
        } catch (error) {}
        process.stdout.write('\x1B[1;32m链接创建中...\x1B[0m');
        loadingSpinner.start(100, {
            clearChar: true
        });
        let stream = fs.createWriteStream(this.downloadFilePath);

        let req = request({
            url: this.url,
            //设置请求头
            headers: {
                'accept': `text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9`,
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'zh-CN,zh;q=0.9',
                ...this.headers
            },
        })
        req.pipe(stream)
        req.on('response', response => {
            let all = response.headers['content-length']
            loadingSpinner.stop();
            progressBar.start(all, 0);
        });
        let cuttent = 0
        req.on('data', chunk => {
            cuttent += chunk.length
            progressBar.update(cuttent);
        });
        req.on("close", err => {
            if (!err) {
                progressBar.stop()
                console.log("下载成功，清除上一版本文件...");
                this.clearFile(this.basePath)
                console.log('解压下载文件...');
                this.fileUnzip()
            } else {
                console.error(err);
            }
        });
    }
    // 文件解压
    fileUnzip() {
        compressing.zip.uncompress(this.downloadFilePath, this.basePath)
            .then(() => {
                this.copyFile()
                console.log('解压完成...');
            })
            .catch(err => {
                console.error(err);
                console.error('文件解压失败，请手动解压');
            })
    }
    // 清除上一版本，文件
    clearFile(path) {
        let files = [];
        if (fs.existsSync(path)) {
            files = fs.readdirSync(path);
            // 当前目录是空就删了
            if (!files.length && path !== this.basePath) {
                fs.rmdirSync(path);
                return
            }
            files.forEach(file => {
                let curPath = path + "/" + file;
                if (fs.statSync(curPath).isDirectory()) {
                    //递归删除文件夹
                    this.clearFile(curPath);
                } else {
                    // 避免删除刚下载的压缩文件
                    if (curPath.endsWith(this.fileName)) return
                    // 删除上次解压文件
                    if (this.whiteArr.some(el => curPath.endsWith('/' + el) || curPath.endsWith('\\' + el))) {
                        fs.unlinkSync(curPath); //删除文件
                    }
                    // 避免删除最外层
                    if (path !== this.basePath) {
                        fs.rmdirSync(path);
                    }
                }
            });
        }
    }
    // 文件复制
    copyFile() {
        let files = fs.readdirSync(this.basePath);
        // 找到当前解压出来的文件font_2294065_d0por6jrwp5，以font_开头的
        let srcPath = files.filter(file => file.startsWith('font_'))[0]
        if (!srcPath) return console.log('未找到解压后文件...')
        let sourcePath = path.join(this.basePath, srcPath)
        let sourceFiles = fs.readdirSync(sourcePath);
        sourceFiles.forEach(file => {
            let oldPath = path.join(sourcePath, file)
            let newPath = path.join(this.basePath, file)
            fs.renameSync(oldPath, newPath)
        })
        fs.rmdirSync(sourcePath); // 删除空文件夹
        fs.unlinkSync(this.downloadFilePath); // 删除下载文件
    }
}

module.exports = Upicon