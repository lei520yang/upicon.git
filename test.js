const Upicon = require('./index.js')
const path = require('path')
const upicon = new Upicon({
    // icon save file 
    basePath: path.join(__dirname, '/aa'),
    // download file name
    fileName: 'download.zip',
    // icon的下载地址
    url: 'https://www.iconfont.cn/api/project/download.zip?spm=a313x.7781069.1998910419.d7543c303&pid=1077752&ctoken=yIWU9VXjQjYX8O6fIbP842aC',
    // 你的登陆cookie
    headers: {
        'cookie': ''
    }
})
upicon.downloadFile()