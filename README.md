###### npm i upicon -D

###### https://gitee.com/lei520yang/upicon.git

###### 使用登陆阿里图标库的网页cookie+下载zip文件的下载链接就可以自动更新阿里图表库

###### You can automatically update the Ali chart library by using the webpage cookie logging in to the Ali Icon Library and the download link to download the zip file

    <!--  use method -->
        const path = require('path')
        const Upicon = require('upicon')

        const upicon = new Upicon({
    <!--     icon save file -->
        basePath:path.join(__dirname, 'aa'),
    <!-- download file name -->
        fileName:'download.zip',
    <!-- icon download address -->
        url: 'https://www.iconfont.cn/api/project/download.zip?spm=a313x.7781069.1998910419.d7543c303&pid=1077752&ctoken=yIWU9VXjQjYX8O6fIbP842aC',
    <!-- your login cookie -->
        headers: {
        'cookie': ''}
        })
        upicon.downloadFile()